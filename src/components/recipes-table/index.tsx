import {
  TableContainer,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  Table,
  TableBody,
  Button,
  ButtonGroup,
  Container,
  Rating,
} from '@mui/material';
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import { FC, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Recipe } from '../../core/models';

const defaultColumns: ColumnDef<Recipe>[] = [
  {
    accessorKey: 'title',
    header: 'Nombre',
    footer: info => info.column.id,
  },
  {
    accessorKey: 'ingredients',
    header: 'Ingredientes',
    footer: info => info.column.id,
  },
  {
    accessorKey: 'category',
    header: 'Categoria',
    footer: info => info.column.id,
  },
  {
    accessorKey: 'difficulty',
    header: 'Dificultad',
    cell: info => <Rating name="simple-controlled" value={info.getValue()} />,
    footer: info => info.column.id,
  },
];

const useColumns: () => ColumnDef<Recipe>[] = () => {
  return defaultColumns;
};

interface RecipesTable {
  data?: Recipe[];
  isLoading: boolean;
  isError: boolean;
  refetch: () => void;
  handleEdit?: (recipe: Recipe) => void;
  handleDelete?: (id: string) => void;
}

export const RecipesTable: FC<RecipesTable> = ({
  data,
  isLoading,
  isError,
  refetch,
  handleEdit,
  handleDelete,
}) => {
  const [sorting, setSorting] = useState<SortingState>([]);
  const columns = useColumns();
  const { pathname } = useLocation();
  const table = useReactTable({
    data: data ?? [],
    columns,
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
  });

  if (isLoading) {
    return <div>Cargando Tabla</div>;
  }

  if (isError) {
    return (
      <Container>
        <span>Ocurrio un error</span>
        <Button onClick={() => refetch()}>Reintentar</Button>
      </Container>
    );
  }

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          {table.getHeaderGroups().map(headerGroup => (
            <TableRow key={headerGroup.id}>
              {headerGroup.headers.map(header => (
                <TableCell key={header.id} sx={{ textAlign: 'center' }}>
                  <div
                    {...{
                      className: header.column.getCanSort()
                        ? 'cursor-pointer select-none'
                        : '',
                      onClick: header.column.getToggleSortingHandler(),
                    }}>
                    {flexRender(
                      header.column.columnDef.header,
                      header.getContext(),
                    )}
                    {{
                      asc: ' 🔼',
                      desc: ' 🔽',
                    }[header.column.getIsSorted() as string] ?? null}
                  </div>
                </TableCell>
              ))}

              <TableCell sx={{ textAlign: 'center' }}>Acciones</TableCell>
            </TableRow>
          ))}
        </TableHead>
        <TableBody>
          {table.getRowModel().rows.map(row => (
            <TableRow
              key={row.id}
              sx={{
                '&:last-child td, &:last-child th': { border: 0 },
                justifyContent: 'center',
              }}>
              {row.getVisibleCells().map(cell => (
                <TableCell key={cell.id} sx={{ textAlign: 'center' }}>
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </TableCell>
              ))}
              <TableCell sx={{ textAlign: 'center' }}>
                <ButtonGroup variant="text" aria-label="outlined button group">
                  {pathname === '/dashboard/recipes' && (
                    <>
                      <Button onClick={() => handleEdit?.(row.original)}>
                        🖊️
                      </Button>
                      <Button onClick={() => handleDelete?.(row.original._id)}>
                        ❌
                      </Button>
                    </>
                  )}
                </ButtonGroup>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
