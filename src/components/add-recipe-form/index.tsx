import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  FormGroup,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormControlLabel,
  Checkbox,
  DialogActions,
  Button,
} from '@mui/material';
import { FormikProps, useFormik } from 'formik';
import { FC } from 'react';
import { useUser } from '../../core/helpers/auth-context';
import { Category, Difficulty, Recipe } from '../../core/models';

type FormValues = Omit<Recipe, '_id'>;
type UseRecipeForm = (
  onSubmit: (value: FormValues) => void,
) => FormikProps<FormValues>;

const useRecipeForm: UseRecipeForm = onSubmit => {
  const { user } = useUser();
  const formik: FormikProps<FormValues> = useFormik<FormValues>({
    initialValues: {
      user_id_originante: user?._id,
      category: Category.OTROS,
      difficulty: Difficulty.BEGINNER,
      ingredients: '',
      title: '',
      procedure: '',
      published: false,
    },
    onSubmit: (values, { resetForm }) => {
      console.log(values)
      onSubmit(values);
      resetForm();
    },
  });

  return formik;
};

interface RecipeFormProps {
  open: boolean;
  toggleModal: () => void;
  onSubmit: (value: FormValues) => void;
}

export const AddRecipeForm: FC<RecipeFormProps> = ({
  open,
  toggleModal,
  onSubmit,
}) => {
  const { handleChange, values, handleSubmit } = useRecipeForm(onSubmit);

  return (
    <Dialog open={open} onClose={toggleModal} fullWidth>
      <DialogTitle>Nueva Receta</DialogTitle>

      <form onSubmit={handleSubmit}>
        <DialogContent>
          <DialogContentText>
            Anade una nueva receta a tu perfil
          </DialogContentText>
          <FormGroup>
            <TextField
              sx={{
                mt: 2,
              }}
              autoFocus
              margin="dense"
              id="title"
              label="Nombre"
              type="text"
              fullWidth
              variant="standard"
              value={values.title}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              id="ingredients"
              label="Ingredientes"
              type="text"
              fullWidth
              variant="standard"
              value={values.ingredients}
              onChange={handleChange}
            />
            <TextField
              sx={{
                mb: 4,
              }}
              autoFocus
              margin="dense"
              id="procedure"
              label="Procedimiento"
              type="text"
              fullWidth
              variant="standard"
              value={values.procedure}
              onChange={handleChange}
            />
            <TextField
              sx={{
                mb: 4,
              }}
              autoFocus
              margin="dense"
              id="pictures"
              label="URL de portada"
              type="text"
              fullWidth
              variant="standard"
              value={values.pictures}
              onChange={handleChange}
            />
            <FormControl
              fullWidth
              sx={{
                mb: 4,
              }}>
              <InputLabel id="category-select-label">Categoria</InputLabel>
              <Select
                labelId="category-select-label"
                id="category"
                name="category"
                value={values.category}
                label="Categoria"
                onChange={handleChange}>
                <MenuItem value={Category.CARNES}>CARNES</MenuItem>
                <MenuItem value={Category.ENSALADA}>ENSALADA</MenuItem>
                <MenuItem value={Category.GLUTEN_FREE}>
                  LIBRE DE GLUTEN
                </MenuItem>
                <MenuItem value={Category.GUISO}>GUISO</MenuItem>
                <MenuItem value={Category.POSTRE}>POSTRE</MenuItem>
                <MenuItem value={Category.SOPA}>SOPA</MenuItem>
                <MenuItem value={Category.VEGETARIANO}>VEGETARIANO</MenuItem>
                <MenuItem value={Category.OTROS}>OTROS</MenuItem>
              </Select>
            </FormControl>
            <FormControl
              fullWidth
              sx={{
                mb: 4,
              }}>
              <InputLabel id="difficulty-select-label">Dificultad</InputLabel>
              <Select
                labelId="difficulty-select-label"
                id="difficulty"
                name="difficulty"
                value={values.difficulty}
                label="Dificultad"
                onChange={handleChange}>
                <MenuItem value={Difficulty.BEGINNER}>Principiante</MenuItem>
                <MenuItem value={Difficulty.EASY}>Facil</MenuItem>
                <MenuItem value={Difficulty.MEDIUM}>Intermedia</MenuItem>
                <MenuItem value={Difficulty.HARD}>Dificil</MenuItem>
                <MenuItem value={Difficulty.MASTER}>Maestro</MenuItem>
              </Select>
            </FormControl>
            <FormControlLabel
              control={
                <Checkbox
                  id="published"
                  checked={values.published}
                  onChange={handleChange}
                />
              }
              label="Publico"
            />
          </FormGroup>
        </DialogContent>
        <DialogActions>
          <Button onClick={toggleModal}>Cancelar</Button>
          <Button type="submit">Guardar</Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
