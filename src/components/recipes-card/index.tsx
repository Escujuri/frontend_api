import {
  Container,
  Button,
  Card,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Grid,
  Rating,
} from '@mui/material';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { Recipe } from '../../core/models';

interface CardRecipes {
  data?: Recipe[];
  isLoading: boolean;
  isError: boolean;
  refetch: () => void;
}

export const RecipeCards: FC<CardRecipes> = ({
  data,
  isLoading,
  isError,
  refetch,
}) => {
  const navigate = useNavigate();

  if (isLoading || !data) {
    return <div>Cargando Recetas</div>;
  }

  if (isError) {
    return (
      <Container>
        <span>Ocurrio un error</span>
        <Button onClick={() => refetch()}>Reintentar</Button>
      </Container>
    );
  }

  return (
    <Grid container spacing={2}>
      {data.map((element, index) => (
        <Grid item key={index} xs={12} sm={3}>
          <Card sx={{ maxWidth: 345 }}>
            <CardMedia
              component="img"
              height="140"
              image={element.pictures}
              alt="imagen"
              onClick={() => {
                navigate('/recipes/' + element._id, {
                  state: {
                    recipe: element,
                  },
                });
              }}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                {element.title}
              </Typography>
              <Typography variant="body2" color="text.secondary" sx={{ mb: 4 }}>
                {element.procedure}
              </Typography>
              <Typography component="legend" sx={{ mt: 2 }}>
                Categoria: <b>{element.category}</b>
              </Typography>

              <Grid container>
                <Grid item>
                  <Typography component="legend" sx={{ mt: 2 }}>
                    Dificultad
                  </Typography>
                </Grid>
                <Grid item>
                  <Rating
                    name="simple-controlled"
                    value={element.difficulty}
                    sx={{ mt: 2 }}
                    disabled
                  />
                </Grid>
              </Grid>
            </CardContent>
            <CardActions>
              <Button
                onClick={() => {
                  navigate('/recipes/' + element._id, {
                    state: {
                      recipe: element,
                    },
                  });
                }}
                size="small">
                Ver detalle
              </Button>
            </CardActions>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};
