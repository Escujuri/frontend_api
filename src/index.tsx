import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import './index.css';
import { ThemeProvider } from '@mui/material/styles';
import React, { FC } from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { QueryProvider } from './core/clients/query';
import { theme } from './core/helpers/theme';
import { DashboardView } from './modules/logged/dashboard';
import { ProfileView } from './modules/logged/profile';
import { ForgotPassword } from './modules/unlogged/forgot-password';
import { HomeView } from './modules/unlogged/home';
import { SignInView } from './modules/unlogged/sign-in';
import { SignUpView } from './modules/unlogged/sign-up';
import reportWebVitals from './reportWebVitals';

import { AuthProvider, AuthRequired } from './core/helpers/auth-context';
import { RecipeDetailView } from './modules/unlogged/detail';

const App: FC = () => (
  <ThemeProvider theme={theme}>
    <QueryProvider>
      <AuthProvider>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
          <Routes>
            <Route path="/" element={<HomeView />} />
            <Route path="/recipes/:id" element={<RecipeDetailView />} />
            <Route path="users">
              <Route path="sign-in" element={<SignInView />} />
              <Route path="sign-up" element={<SignUpView />} />
              <Route path="forgot-password" element={<ForgotPassword />} />
            </Route>
            <Route path="dashboard">
              <Route
                path="recipes"
                element={
                  <AuthRequired>
                    <DashboardView />
                  </AuthRequired>
                }
              />
              <Route
                path="profile"
                element={
                  <AuthRequired>
                    <ProfileView />
                  </AuthRequired>
                }
              />
            </Route>
          </Routes>
        </BrowserRouter>
      </AuthProvider>
    </QueryProvider>
  </ThemeProvider>
);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement,
);

if (process.env.NODE_ENV === 'development') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const { worker } = require('./mocks/browser');
  sessionStorage.setItem('recipes', JSON.stringify([]));
  worker.start();
}

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
