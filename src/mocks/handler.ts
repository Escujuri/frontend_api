import { rest } from 'msw';
import { Category, Difficulty, Recipe } from '../core/models';

const RECIPES_MOCKS = [
  {
    _id: Math.random().toString(),
    title: 'Nombre',
    ingredients: 'arroz huevo sal',
    procedure: 'lorem ipsum',
    published: true,
    difficulty: Difficulty.EASY,
    category: Category.OTROS,
  },
  {
    _id: Math.random().toString(),
    title: 'Nombre 2',
    ingredients: 'arroz huevo sal',
    procedure: 'lorem ipsum',
    published: true,
    difficulty: Difficulty.EASY,
    category: Category.OTROS,
  },
  {
    _id: Math.random().toString(),
    title: 'Nombre 3',
    ingredients: 'arroz huevo sal',
    procedure: 'lorem ipsum',
    published: true,
    difficulty: Difficulty.EASY,
    category: Category.OTROS,
  },
  {
    _id: Math.random().toString(),
    title: 'Nombre 4',
    ingredients: 'arroz huevo sal',
    procedure: 'lorem ipsum',
    published: true,
    difficulty: Difficulty.EASY,
    category: Category.OTROS,
  },
];

export const handlers = [
  rest.post('/api/users/login', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        token: 'token',
        user: {
          id: 'id-test',
          email: 'test@test.com',
          firstName: 'test',
          lastName: 'test',
        },
      }),
    );
  }),
  rest.post('/api/users/register', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        token: 'token',
        user: {
          id: 'id-test',
          email: 'test@test.com',
          firstName: 'test',
          lastName: 'test',
        },
      }),
    );
  }),
  rest.post('/api/users/logout', (_req, res, ctx) => {
    sessionStorage.clear();

    return res(ctx.status(200));
  }),
  rest.post('/users/forget', (_req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        id: 'id-test',
        email: 'test@test.com',
      }),
    );
  }),
  rest.get('/api/recipes', (_req, res, ctx) => {
    return res(
      ctx.status(200),
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      ctx.json(JSON.parse(sessionStorage.getItem('recipes')!)),
    );
  }),
  rest.get('/api/recipes/user', (req, res, ctx) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const recipes = JSON.parse(sessionStorage.getItem('recipes')!) as Recipe[];
    return res(ctx.status(200), ctx.json(recipes));
  }),
  rest.post('/api/users/recipes/new', (req, res, ctx) => {
    const randomRecipe = {
      ...RECIPES_MOCKS[Math.floor(Math.random() * 4) + 0],
      user_id_originante: 'asdadasdasd',
    };
    sessionStorage.setItem(
      'recipes',
      JSON.stringify([
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        ...JSON.parse(sessionStorage.getItem('recipes')!),
        randomRecipe,
      ]),
    );
    return res(
      ctx.status(200),
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      ctx.json(JSON.parse(sessionStorage.getItem('recipes')!)),
    );
  }),
];
