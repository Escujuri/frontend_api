interface Identificable {
  _id: string;
}

export interface User extends Identificable {
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
}

export enum Category {
  POSTRE = 'Poster',
  ENSALADA = 'Ensalada',
  SOPA = 'Sopa',
  GUISO = 'Guiso',
  CARNES = 'Carnes',
  GLUTEN_FREE = 'Libre gluten',
  VEGETARIANO = 'Vegetariano',
  OTROS = 'Otros',
}

export enum Difficulty {
  BEGINNER,
  EASY,
  MEDIUM,
  HARD,
  MASTER,
}

export interface Recipe extends Identificable {
  title: string;
  difficulty: Difficulty;
  category: Category;
  ingredients: string;
  procedure: string;
  published: boolean;
  pictures?: string;
  user_id_originante?: string;
}

export type RecipesResponse = { data: { docs: Recipe[] } };
