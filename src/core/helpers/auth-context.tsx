import {
  createContext,
  FC,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { Navigate, useLocation, useNavigate } from 'react-router-dom';
import { User } from '../models';

interface AuthUser {
  token?: string;
  user?: User;
  clearStorage: () => void;
  setUser: (user: { token: string; user: User }) => void;
}

type UseUser = () => AuthUser & { isAuthenticated: boolean };

export const AuthContext = createContext<AuthUser>({} as AuthUser);

export const AuthProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [defaultValue, setDefaultValue] = useState<
    Omit<AuthUser, 'clearStorage' | 'setUser'>
  >({});

  const clearStorage = useCallback(() => {
    sessionStorage.clear();
    setDefaultValue({});
  }, [setDefaultValue]);

  const setUser = useCallback((user: { token: string; user: User }) => {
    sessionStorage.setItem('user', JSON.stringify(user));
    setDefaultValue({ ...defaultValue, ...user });
  }, []);

  useEffect(() => {
    const userDataRaw = sessionStorage.getItem('user');

    if (userDataRaw) {
      setDefaultValue(JSON.parse(userDataRaw));
    }
  }, []);

  return (
    <AuthContext.Provider value={{ ...defaultValue, clearStorage, setUser }}>
      {children}
    </AuthContext.Provider>
  );
};

export const AuthRequired: FC<{ children: JSX.Element }> = ({ children }) => {
  const user = useContext(AuthContext);
  const location = useLocation();

  if (user.token) {
    return children;
  }

  return <Navigate to="/users/sign-in" state={{ from: location }} replace />;
};

export const useUser: UseUser = () => {
  const user = useContext(AuthContext);

  return { ...user, isAuthenticated: !!user.token };
};

export const useNotAuthenticated: () => void = () => {
  const navigate = useNavigate();
  const { isAuthenticated } = useUser();

  useEffect(() => {
    if (isAuthenticated) {
      navigate('/dashboard/recipes', { replace: true });
    }
  }, [isAuthenticated]);
};
