interface FetchClient {
  post: <T extends Response>(
    url: string,
    data?: BodyInit | null,
    token?: string,
  ) => Promise<T>;
  put: <T extends Response>(
    url: string,
    data?: BodyInit | null,
    token?: string,
  ) => Promise<T>;
  get: <T extends Response>(url: string, token?: string) => Promise<T>;
  delete: (url: string, token?: string) => Promise<Response>;
}

type HeaderAuthorization = (token?: string) => HeadersInit;

const headerAuthorization: HeaderAuthorization = token => {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  if (token) {
    headers.append('x-access-token', token);
  }
  return headers;
};

export const fetchClient: FetchClient = {
  post: (url, data, token) =>
    fetch('http://localhost:4000/api' + url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headerAuthorization(token),
    }).then(datas => datas.json()),
  put: (url, data, token) =>
    fetch('http://localhost:4000/api' + url, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: headerAuthorization(token),
    }).then(datas => datas.json()),
  get: (url, token) =>
    fetch('http://localhost:4000/api' + url, {
      method: 'GET',
      headers: headerAuthorization(token),
    }).then(datas => datas.json()),
  delete: (url, token) =>
    fetch('http://localhost:4000/api' + url, {
      method: 'DELETE',
      headers: headerAuthorization(token),
    }),
};
