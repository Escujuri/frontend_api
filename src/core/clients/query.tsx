import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { FC } from 'react';

export const queryClient = new QueryClient();
export const QueryProvider: FC<{ children?: JSX.Element }> = ({ children }) => (
  <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
);
