import { useMutation } from '@tanstack/react-query';
import { FormikProps, useFormik } from 'formik';
import { fetchClient } from '../../../core/clients/fetch';
import { User } from '../../../core/models';

interface FormValues extends Omit<User, '_id'> {
  password?: string;
}

type UseSignUpViewModel = (options: { onSuccess: () => void }) => FormikProps<FormValues>;

export const useSignUpViewModel: UseSignUpViewModel = ({ onSuccess }) => {
  const signUpMutation = useMutation((userData: FormValues) =>
    fetchClient.post('/users/register', userData as unknown as BodyInit),
  );
  const formik: FormikProps<FormValues> = useFormik<FormValues>({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
    },
    onSubmit: async (values) => {
      try {
        const signUpResult = await signUpMutation.mutateAsync(values);

        if (signUpResult) {
          sessionStorage.setItem(
            'user',
            JSON.stringify({
              token: 'token',
              user: {
                email: 'test@test.com',
                firstName: 'test',
                lastName: 'test',
              },
            }),
          );
          onSuccess();
        }
      } catch (e) {
        alert('ocurrio un error');
        console.error(e);
      }
    },
  });

  return formik;
};
