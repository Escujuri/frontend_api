import SoupKitchen from '@mui/icons-material/SoupKitchen';
import {
  CssBaseline,
  AppBar,
  Toolbar,
  Typography,
  Divider,
  Rating,
} from '@mui/material';
import { Box, Container } from '@mui/system';
import { FC } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Recipe } from '../../../core/models';

export const RecipeDetailView: FC = () => {
  const navigate = useNavigate();
  const { state } = useLocation();
  const {
    recipe: { title, ingredients, procedure, category, difficulty, pictures },
  } = state as { recipe: Recipe };

  return (
    <>
      <CssBaseline />
      <AppBar component="nav">
        <Toolbar>
          <SoupKitchen
            onClick={() => navigate('/')}
            sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }}
          />
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}>
            TusRecetas
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        <Box
          sx={{
            pt: 15,
            pb: 6,
          }}>
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom>
              {title}
            </Typography>
          </Container>
        </Box>
        <Container maxWidth="xl">
          <Container maxWidth="xl" sx={{ mb: 4, mt: 4 }}>
            <Typography variant="h6">Ingredientes</Typography>
            <Divider sx={{ mb: 2, mt: 2 }} />
            <Typography variant="body1">{ingredients}</Typography>
          </Container>

          <Container maxWidth="xl" sx={{ mb: 4, mt: 4 }}>
            <Typography variant="h6">Procedimiento</Typography>
            <Divider sx={{ mb: 2, mt: 2 }} />
            <Typography variant="body1">{procedure}</Typography>
          </Container>

          <Container maxWidth="xl" sx={{ mb: 4, mt: 4 }}>
            <Typography variant="h6">Categoria</Typography>
            <Divider sx={{ mb: 2, mt: 2 }} />
            <Typography variant="body1">{category}</Typography>
          </Container>

          <Container maxWidth="xl" sx={{ mb: 4, mt: 4 }}>
            <Typography variant="h6">Dificultad</Typography>

            <Divider sx={{ mb: 2, mt: 2 }} />
            <Rating name="simple-controlled" value={difficulty} disabled />
          </Container>

          <Container maxWidth="xl">
            <img src={pictures} />
          </Container>
        </Container>
      </main>
    </>
  );
};
