import { useMutation } from '@tanstack/react-query';
import { FormikProps, useFormik } from 'formik';
import { fetchClient } from '../../../core/clients/fetch';

interface FormValues {
  email: string;
}

type UseForgotPasswordViewModel = (options: { onSuccess: () => void }) => FormikProps<FormValues>;

export const useForgotPasswordViewModel: UseForgotPasswordViewModel = ({
  onSuccess,
}) => {
  const forgetMutation = useMutation((userData: FormValues) =>
    fetchClient.post('/users/forget', userData as unknown as BodyInit),
  );
  const formik: FormikProps<FormValues> = useFormik<FormValues>({
    initialValues: {
      email: ''
    },
    onSubmit: async (values) => {
      try {
        const forgotResult = await forgetMutation.mutateAsync(values);
  
        if (forgotResult) {
          onSuccess();
        }
      } catch (e) {
        alert('ocurrio un error');
        console.error(e);
      }
    },
  });

  return formik;
};
