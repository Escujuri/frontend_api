import { useMutation } from '@tanstack/react-query';
import { FormikProps, useFormik } from 'formik';
import { fetchClient } from '../../../core/clients/fetch';
import { useUser } from '../../../core/helpers/auth-context';
import { User } from '../../../core/models';

interface FormValues {
  email: string;
  password: string;
}

type UseSignInViewModel = (options: {
  onSuccess: () => void;
}) => FormikProps<FormValues>;

type LoginResponse = {
  loginUser: { token: string; user: User };
} & Response;

export const useSignInViewModel: UseSignInViewModel = ({ onSuccess }) => {
  const { setUser } = useUser();
  const signInMutation = useMutation((userData: FormValues) =>
    fetchClient.post<LoginResponse>(
      '/users/login/',
      userData as unknown as BodyInit,
    ),
  );
  const formik: FormikProps<FormValues> = useFormik<FormValues>({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: values => {
      const { password, email } = values;

      signInMutation.mutate(
        { email, password },
        {
          onSuccess: signInResult => {
            const { loginUser } = signInResult;
            if (loginUser) {
              setUser(loginUser);
              onSuccess();
            }
          },
          onError: () => {
            alert('Ocurrio un errror');
          },
        },
      );
    },
  });

  return formik;
};
