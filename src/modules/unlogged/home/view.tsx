import SoupKitchen from '@mui/icons-material/SoupKitchen';
import {
  CssBaseline,
  AppBar,
  Toolbar,
  Typography,
  Box,
  Container,
  Stack,
  Button,
  Divider,
  TextField,
  Grid,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';
import { useQuery } from '@tanstack/react-query';
import {
  ChangeEventHandler,
  ReactNode,
  useEffect,
  useState,
  type FC,
} from 'react';
import { useNavigate } from 'react-router-dom';
import { RecipeCards } from '../../../components/recipes-card';
import { fetchClient } from '../../../core/clients/fetch';
import { Recipe, RecipesResponse } from '../../../core/models';

export const HomeView: FC = () => {
  const navigate = useNavigate();
  const [search, setSearch] = useState('');
  const [property, setProperty] =
    useState<keyof Pick<Recipe, 'title' | 'category' | 'ingredients'>>('title');
  const { isLoading, isError, data, refetch } = useQuery(['getRecipes'], () =>
    fetchClient.get<RecipesResponse & Response>('/recipes'),
  );
  const [dataRaw, setDataRaw] = useState<Recipe[] | undefined>();

  const handleSearch: ChangeEventHandler<HTMLInputElement> = event => {
    setSearch(event.target.value);
  };

  const handleProperty: (
    event: SelectChangeEvent<'title' | 'category' | 'ingredients'>,
    child: ReactNode,
  ) => void = event => {
    setProperty(
      event.target.value as keyof Pick<
        Recipe,
        'title' | 'category' | 'ingredients'
      >,
    );
  };

  useEffect(() => {
    if (!isLoading) {
      refetch();
    }
  }, []);

  useEffect(() => {
    if (data?.data.docs !== dataRaw) {
      setDataRaw(data?.data.docs);
    }
  }, [data, dataRaw]);

  return (
    <>
      <CssBaseline />
      <AppBar component="nav">
        <Toolbar>
          <SoupKitchen
            onClick={() => navigate('/')}
            sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }}
          />
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}>
            TusRecetas
          </Typography>

          <Box sx={{ display: { xs: 'none', sm: 'block' }, ml: 5 }}>
            <Button
              onClick={() => navigate('/users/sign-in')}
              sx={{ color: '#fff' }}>
              Ingresar
            </Button>
            <Button
              onClick={() => navigate('/users/sign-up')}
              sx={{ color: '#fff' }}>
              Registrate
            </Button>
          </Box>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 15,
            pb: 6,
          }}>
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom>
              Nuevas Recetas!
            </Typography>

            <Stack
              sx={{ pt: 4 }}
              direction="row"
              spacing={2}
              justifyContent="center">
              <Grid container spacing={2}>
                <Grid item xs={6} sm={4}>
                  <FormControl fullWidth>
                    <InputLabel id="search-category-label">Criterio</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={property}
                      label="Criterio"
                      onChange={handleProperty}>
                      <MenuItem value="title">Titulo</MenuItem>
                      <MenuItem value="category">Categoria</MenuItem>
                      <MenuItem value="ingredients">Ingredientes</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={6} sm={8}>
                  <TextField
                    id="search"
                    type="text"
                    onChange={handleSearch}
                    label="Buscar Receta:"
                    fullWidth
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Stack>
          </Container>
        </Box>
        <Container maxWidth="xl">
          <Divider sx={{ mt: 5, mb: 10 }} />

          <RecipeCards
            {...{
              data: dataRaw?.filter(item =>
                item[property].toLowerCase().includes(search.toLowerCase()),
              ),
              isLoading,
              isError,
              refetch,
            }}
          />
        </Container>
      </main>
    </>
  );
};
