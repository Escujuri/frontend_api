import { useMutation } from '@tanstack/react-query';
import { FormikProps, useFormik } from 'formik';
import { fetchClient } from '../../../core/clients/fetch';
import { useUser } from '../../../core/helpers/auth-context';
import { User } from '../../../core/models';

type FormValues = Omit<User, '_id' | 'phone' | 'email'> & { password?: string };
type UseProfileViewModel = (params: {
  onSuccess: () => void;
}) => FormikProps<FormValues>;

export const useProfileViewModel: UseProfileViewModel = ({ onSuccess }) => {
  const { token, user: userRaw } = useUser();
  const updateMutation = useMutation((userData: FormValues) =>
    fetchClient.put('/users/update', userData as unknown as BodyInit, token),
  );
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const { firstName, lastName } = userRaw!;

  const formik: FormikProps<FormValues> = useFormik<FormValues>({
    initialValues: {
      firstName,
      lastName,
    },
    onSubmit: async values => {
      try {
        const updateResult = await updateMutation.mutateAsync(values);

        if (updateResult) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          //@ts-ignore
          const user = updateResult.data as User;
          sessionStorage.setItem('user', JSON.stringify({ token, user }));
          onSuccess();
        }
      } catch (e) {
        alert('ocurrio un error');
        console.error(e);
      }
    },
  });

  return formik;
};
