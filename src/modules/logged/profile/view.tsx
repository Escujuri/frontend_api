import {
  Box,
  Grid,
  TextField,
  Container,
  Button,
  Card,
  CardContent,
  Divider,
  Typography,
} from '@mui/material';
import { type FC } from 'react';
import { Drawer } from '../../../components/drawer';
import { useProfileViewModel } from './view.model';

export const ProfileView: FC = () => {
  const { handleSubmit, handleChange, values } = useProfileViewModel({
    onSuccess: () => {
      alert('Perfil editado');
    },
  });

  return (
    <Drawer>
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Card>
          <CardContent>
            <Typography variant="h5" sx={{ mb: 1 }}>
              Perfil
            </Typography>
            <Divider />
            <Box
              component="form"
              noValidate
              onSubmit={handleSubmit}
              sx={{ mt: 3 }}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="given-name"
                    name="firstName"
                    required
                    fullWidth
                    id="firstName"
                    label="Nombre"
                    value={values.firstName}
                    onChange={handleChange}
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="lastName"
                    label="Apellido"
                    name="lastName"
                    value={values.lastName}
                    onChange={handleChange}
                    autoComplete="family-name"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Contrasena"
                    type="password"
                    id="password"
                    value={values.password}
                    onChange={handleChange}
                    autoComplete="new-password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}>
                    Editar perfil
                  </Button>
                </Grid>
              </Grid>
            </Box>
          </CardContent>
        </Card>
      </Container>
    </Drawer>
  );
};
