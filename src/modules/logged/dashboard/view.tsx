import { Box, Button, Container } from '@mui/material';
import { type FC } from 'react';
import { Drawer } from '../../../components/drawer';
import { AddRecipeForm } from '../../../components/add-recipe-form';
import { RecipesTable } from '../../../components/recipes-table';
import { useDashboardViewModel } from './view.model';
import { EditRecipeForm } from '../../../components/edit-recipe-form';

export const DashboardView: FC = () => {
  const {
    modal: [open, toggle],
    editModal: [editOpen, toggleEdit],
    add,
    edit,
    remove,
    recipeForEdit: recipe,
    recipesData: { data, isLoading, isError, refetch },
  } = useDashboardViewModel();

  return (
    <Drawer>
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
            mb: 4,
          }}>
          <Button onClick={toggle} variant="contained">
            ➕ Anadir receta
          </Button>
        </Box>
        <RecipesTable
          {...{
            data: data?.data.docs ?? [],
            isLoading,
            isError,
            refetch,
            handleDelete: remove,
            handleEdit: toggleEdit,
          }}
        />

        <AddRecipeForm onSubmit={add} open={open} toggleModal={toggle} />

        {!!recipe && <EditRecipeForm
          onSubmit={edit}
          open={editOpen && !!recipe}
          toggleModal={toggleEdit}
          recipe={recipe}
        />}
      </Container>
    </Drawer>
  );
};
