import { useMutation, useQuery, UseQueryResult } from '@tanstack/react-query';
import { useState, useCallback } from 'react';
import { fetchClient } from '../../../core/clients/fetch';
import { useUser } from '../../../core/helpers/auth-context';
import { Recipe, RecipesResponse } from '../../../core/models';

type MutateAction<T> = (values: T) => Promise<void>;
type AddMutation = MutateAction<Omit<Recipe, '_id'>>;
type DeleteMutation = (id: string) => void;
type EditMutation = (value: Partial<Recipe>) => void;

interface UseDashboardViewModelResult {
  modal: [boolean, () => void];
  editModal: [boolean, (recipe?: Recipe) => void];
  recipeForEdit?: Recipe;
  add: AddMutation;
  remove: DeleteMutation;
  edit: EditMutation;
  recipesData: UseQueryResult<RecipesResponse>;
}

type UseDashboardViewModel = () => UseDashboardViewModelResult;

export const useDashboardViewModel: UseDashboardViewModel = () => {
  const { token } = useUser();
  const [recipe, setRecipe] = useState<Recipe | undefined>();
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [modalEditOpen, setModalEditOpen] = useState<boolean>(false);
  const recipesData = useQuery(['getUserRecipes'], () =>
    fetchClient.get<RecipesResponse & Response>('/recipes/user', token),
  );
  const addMutation = useMutation((userData: Omit<Recipe, '_id'>) =>
    fetchClient.post('/recipes/new', userData as unknown as BodyInit, token),
  );
  const editMutation = useMutation((userData: Partial<Recipe>) =>
    fetchClient.put('/recipes/update', userData as unknown as BodyInit, token),
  );
  const removeMutation = useMutation((id: string) =>
    fetchClient.delete('/recipes/delete/' + id, token),
  );

  const handleToggleModal = useCallback(() => {
    setModalOpen(prevValue => !prevValue);
  }, [setModalOpen]);
  const handleToggleEditModal = useCallback(
    (recipeData?: Recipe) => {
      setRecipe(() => recipeData);
      setModalEditOpen(prevValue => !prevValue);
    },
    [setModalOpen],
  );

  const add: AddMutation = useCallback(
    async values => {
      const results = await addMutation.mutateAsync(values);

      if (results) {
        recipesData.refetch();
        handleToggleModal();
      }
    },
    [addMutation, recipesData, handleToggleModal],
  );

  const edit: EditMutation = useCallback(
    async value => {
      const results = await editMutation.mutateAsync(value);

      if (results) {
        recipesData.refetch();
        handleToggleEditModal();
      }
    },
    [editMutation],
  );

  const remove: DeleteMutation = useCallback(
    async value => {
      const results = await removeMutation.mutateAsync(value);

      if (results) {
        recipesData.refetch();
      }
    },
    [editMutation],
  );

  return {
    modal: [modalOpen, handleToggleModal],
    editModal: [modalEditOpen, handleToggleEditModal],
    add,
    edit,
    remove,
    recipesData,
    recipeForEdit: recipe,
  };
};
